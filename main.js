function updateColor() {
    const red = document.getElementById('red_range').valueAsNumber
    const green = document.getElementById('green_range').valueAsNumber
    const blue = document.getElementById('blue_range').valueAsNumber
    const color_code = `rgb(${red}, ${green}, ${blue})`
    document.getElementById('color').style.backgroundColor = color_code
}

function handleChange(event) {
    const range = event.target
    const monitor = range.nextElementSibling
    monitor.value = range.value
    updateColor()
}

const ranges = document.querySelectorAll('input[type=range]')
for (const range of ranges) {
    range.oninput = handleChange
}

updateColor()
