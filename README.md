# Color composer

This is a little example of how Javascript can respond to user actions, modifing the DOM using its events.

You can play [here](https://color-composer-penascal-frontend-javascript-4a41c862bbe84666626.gitlab.io/) with a live version.